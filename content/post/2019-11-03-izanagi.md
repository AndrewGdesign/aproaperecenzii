---
title: IZANAGI 2.0 - festival de film japonez
date: 2019-11-03
tags:
  ["film", "anime", "japonia", "Paprika", "Ghost in the shell", "time of eve"]
---

![Izanagi](/img/izanagi.jpg "Izanagi")

Salutare, îmi pare rău că de data asta nu revin cu o recenzie, ci mai mult cu un fel de anunț. Da, proiectul care nu credeam că va atrage așa multă atenție se întoarce cu o nouă ediție foarte ambițioasă. Trei zile, trei filme care nu trebuie ratate, trei seri în care ne vom scufunda prin universuri SF, pline de idei filosofice care pot duce la discuții aprinse mult dupa vizionarea filmului.
Cum am spus și la prima ediție, acest festival nu are voie să fie ratat nici acum (el fiind în continuare cu intrare liberă :D) de cinefili, în special de cei pasionați de anime, Japonia și SF.

Vă las aici mai multe detalii legate de program și partenerii oficiali IZANAGI + site-ul oficial

<h3>PROGRAM:</h3>

**22.11.2019, vineri, ora 19:**
Paprika (2006, r. Satoshi Kon)

![Paprika 2006](/img/paprika.png "Paprika 2006")

**23.11.2019, sâmbătă, ora 19:**
Ghost in the Shell (1995, r. Mamoru Oshii) + dezbatere

![Ghost in the Shell 1995](/img/ghost.png "Ghost in the Shell 1995")

**24.11.2019, duminică, ora 19:**
Eve no Jikan (2010, r. Yasuhiro Yoshiura)

![Eve no Jikan 2010](/img/eve.png "Eve no Jikan 2010")

Evenimentul este organizat de <a href="https://prismaproduction.pro/" target="_blank">**PRISMA PRODUCTION PRO**</a>, în colaborare cu Ambasada Japoniei și Centrul de Studii Româno-Japoneze „Angela Hondru”, cu sprijinul <a href="https://www.tipomedia.ro/" target="blank">**Tipomedia.ro**</a> și Spark România.

**Parteneri**: AsiaFest, Cărturești, Anim’est, Animation Worksheep, Anime Culture Club, Sloop, StaticVFX

**Parteneri media**: Cinemap, Zile și Nopți, FILM MENU, Overheat, Raftul cu Idei, FilmSinopsis, Aproape Recenzii, emilcalinescu.eu

<a href="https://izanagi.ro/" target="_blank"><b>izanagi.ro</b></a>

<br>
